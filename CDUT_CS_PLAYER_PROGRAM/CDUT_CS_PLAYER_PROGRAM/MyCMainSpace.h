#pragma once

#include <QMainWindow>
#include "ui_MyCMainSpace.h"

#include "MyStruct.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MyCMainSpaceClass; };
QT_END_NAMESPACE

//extern WinAdjust systemSetting;

class QSettings;
class QSplashScreen;
class MyCSplashScreen;
class MyCProjectSettingWidget;
class MySetting;
class MyCSql;
class MyCSoftwarePage;
class MyXmlDelegate;

class MyCMainSpace : public QMainWindow
{
	Q_OBJECT

private:
	void showSplash();
	void _initAll();
	void initXml();
	void initSettings();
	void initSql();
	void initConnections();
	void syncAll();	//在析构中调用,确定所有需要同步的值已保存,与handle_sync...函数性质不同,为最终生效结果.

public slots:
	void handle_sync_from_setting();	//使在设置窗口的一切更改值,在此函数中立即生效

public:
	MyCMainSpace(QWidget *parent = nullptr);
	~MyCMainSpace();
	
	//QSettings* getSettingInstance();	//弃用的函数,返回该类实例保存的QSettings对象
private:
	//QSettings* m_pSettings;
private:
	Ui::MyCMainSpaceClass *ui;
	QSplashScreen *m_pSplashScreen;
	QSettings* m_pSettings;
	
	MyCSql* m_pMySql;
	//MySetting *m_pMySetting;
	MyXmlDelegate* m_pMyXml;

	MyCSoftwarePage* m_pMySoftwarePage = nullptr;

	WinAdjust m_winAdjust;
	
	
	MyCProjectSettingWidget* m_pProjectSettingWidget = nullptr;
};
