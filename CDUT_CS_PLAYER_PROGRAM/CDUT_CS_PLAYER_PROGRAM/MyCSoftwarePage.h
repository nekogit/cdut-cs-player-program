#pragma once

#include <QDialog>
#include "ui_MyCSoftwarePage.h"

class MyCSoftwarePage : public QDialog
{
	Q_OBJECT

public:
	MyCSoftwarePage(QWidget *parent = nullptr);
	~MyCSoftwarePage();

private:
	Ui::MyCSoftwarePageClass ui;
};
