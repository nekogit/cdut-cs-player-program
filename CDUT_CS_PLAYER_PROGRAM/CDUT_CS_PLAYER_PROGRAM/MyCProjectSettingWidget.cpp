#pragma once

#include "MyCProjectSettingWidget.h"
#include "MyCMainSpace.h"
#include "MySetting.h"
#include <qlistwidget.h>
#include <qsettings.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <qtreewidget.h>
#include <qstring.h>



MyCProjectSettingWidget::MyCProjectSettingWidget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::MyCProjectSettingWidgetClass())
{
	ui->setupUi(this);

	initPage();

	//QTreeWidget* t = ui->page_1_treeWidget;

	//m_pItem = nullptr;

	/*connect(ui->page_1_treeWidget, &QTreeWidget::itemDoubleClicked, this, &MyCProjectSettingWidget::openEditor);
	connect(ui->page_1_treeWidget, &QTreeWidget::itemSelectionChanged, this, &MyCProjectSettingWidget::closeEditor);
	connect(ui->page_1_treeWidget, &QTreeWidget::itemChanged, [=] {
		ui->pushButton_Apply->setEnabled(true);
		});*/

	//connect(ui->listWidget, &QListWidget::currentItemChanged, ui->stackedWidget, &QStackedWidget::setCurrentIndex);
}

MyCProjectSettingWidget::~MyCProjectSettingWidget()
{
	/*if (nullptr != m_pMySetting)
		delete m_pMySetting;*/

	delete ui;
}

void MyCProjectSettingWidget::addItem(QString itemName)
{
	
	QListWidgetItem* Litem = new QListWidgetItem(/*itemName,*/ui->listWidget);
	
	
	//Litem->setTextAlignment(Qt::AlignCenter);
	
	MyListItem *label = new MyListItem(itemName);
	label->setBaseSize(QSize(ui->listWidget->width(), 35));
	label->setAlignment(Qt::AlignCenter);
	
	Litem->setSizeHint(QSize(ui->listWidget->width(), 35));

	label->setVisible(false);
	
	ui->listWidget->addItem(Litem);
	
	m_pListItems.push_back(Litem);
	
	ui->listWidget->setItemWidget(Litem, label);

	label->setIndex(m_pListItems.size() - 1);

	connect(label, &MyListItem::listItemClicked, this, &MyCProjectSettingWidget::do_currentPageChange);
	
}

void MyCProjectSettingWidget::addItems()
{
	QVector<QString> blocks;
	blocks.push_back(QString::fromLocal8Bit("主页"));
	blocks.push_back(QString::fromLocal8Bit("2"));
	blocks.push_back(QString::fromLocal8Bit("3"));
	blocks.push_back(QString::fromLocal8Bit("4"));
	blocks.push_back(QString::fromLocal8Bit("5"));
	blocks.push_back(QString::fromLocal8Bit("6"));

	for (auto item : blocks)
	{
		addItem(item);
	}
}

void MyCProjectSettingWidget::initPage()
{
	//m_pMySetting = nullptr;
	//m_pSettings = nullptr;
	//m_pMySetting = new MySetting();
	//m_pSettings = m_pMySetting->initSetting();

	//放入所有符合的实例对象
	m_pSettingInputs.push_back(ui->page_1_spinBoxMainWidth);
	m_pSettingInputs.push_back(ui->page_1_spinBoxMainHeight);
	m_pSettingInputs.push_back(ui->page_1_spinBoxFontSize);

	for (QObject* obj : m_pSettingInputs)	//对该ui内属性存储对象进行遍历,如若对象满足以下条件(是否为某class实例,则绑定对应的信号,若该实例对象内数值发生变化,则调用函数以使得Apply 按钮可以使用.
	{
		if (obj->metaObject()->className() == QString("QLineEdit"))
		{
			connect((QLineEdit*)obj, &QLineEdit::textChanged, [=] {
				ui->pushButton_Apply->setEnabled(true);
				});
		}
		else if (obj->metaObject()->className() == QString("QSpinBox"))
		{
			connect((QSpinBox*)obj, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=] {
				ui->pushButton_Apply->setEnabled(true);
				});
		}
		
	}

	ui->stackedWidget->setCurrentIndex(0);
	addItems();

	connect(ui->pushButton_Apply, &QPushButton::clicked, [=] {
		ui->pushButton_Apply->setEnabled(false);
		setParams();
		});

	connect(ui->pushButton_Cancel, &QPushButton::clicked, [=] {
		getParams();
		});

	getParams();
}

//获取settings的设置属性,同步更新到设置窗口内
void MyCProjectSettingWidget::getParams()
{
	/*ui->page_1_spinBoxMainWidth->setValue(m_pMySetting->getInt("App/win_width"));
	ui->page_1_spinBoxMainHeight->setValue(m_pMySetting->getInt("App/win_height"));*/

	ui->page_1_spinBoxMainWidth->setValue(systemSetting.win_width);
	ui->page_1_spinBoxMainHeight->setValue(systemSetting.win_height);
	/*int width = settings->value("App/win_width").toInt();
	int height = settings->value("App/win_height").toInt();
	QTreeWidget* t = ui->page_1_treeWidget;
	QTreeWidgetItem* item = new QTreeWidgetItem(t);
	item->setText(1, QString::number(width));*/
	
	ui->pushButton_Apply->setEnabled(false);

}

//设置对应窗体的设置条目,并通知主窗体进行及时更新
void MyCProjectSettingWidget::setParams()
{
	/*m_pMySetting->setInt("App/win_width", ui->page_1_spinBoxMainWidth->value());
	m_pMySetting->setInt("App/win_height", ui->page_1_spinBoxMainHeight->value());

	m_pMySetting->syncSetting();*/

	systemSetting.win_width = ui->page_1_spinBoxMainWidth->value();
	systemSetting.win_height = ui->page_1_spinBoxMainHeight->value();

	emit signal_sync();

}

//void MyCProjectSettingWidget::openEditor(QTreeWidgetItem* item, int column)
//{
//	/*if (column != 0)
//	{
//		ui->page_1_treeWidget->openPersistentEditor(item, column);
//		m_pItem = item;
//		mColumn = column;
//	}*/
//	
//	
//}

//void MyCProjectSettingWidget::closeEditor()
//{
//	/*if(m_pItem != nullptr)
//	ui->page_1_treeWidget->closePersistentEditor(m_pItem, mColumn);*/
//
//}

void MyCProjectSettingWidget::do_currentPageChange(int index)
{
	ui->stackedWidget->setCurrentIndex(index);
}
