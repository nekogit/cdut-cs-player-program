#pragma once

#include <QWidget>
#include <qvector.h>
#include "ui_MyCProjectSettingWidget.h"
#include "MyStruct.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MyCProjectSettingWidgetClass; };
QT_END_NAMESPACE

//extern WinAdjust systemSetting;

class MyListItem;
class QListWidgetItem;
class QSettings;
class MySetting;
class MyCProjectSettingWidget : public QWidget
{
	Q_OBJECT

private:
	void addItem(QString itemName);
	void addItems();
	void initPage();
	void getParams();
	void setParams();
public slots:
	void do_currentPageChange(int index);
	//void openEditor(QTreeWidgetItem* item, int column);
	//void closeEditor();

public:
	MyCProjectSettingWidget(QWidget *parent = nullptr);
	~MyCProjectSettingWidget();

signals:
	void signal_sync();	//通知主窗体对设置属性及时更新

private:
	Ui::MyCProjectSettingWidgetClass *ui;

	//MySetting* m_pMySetting;
	//QSettings* m_pSettings;
	QVector<QListWidgetItem*> m_pListItems;
	//QTreeWidgetItem* m_pItem;
	QVector<QObject*> m_pSettingInputs;	//保存设置窗口内的属性对应的值ui对象接口
	//int mColumn;
};

#include <qlabel.h>
#include <QMouseEvent>
class MyListItem : public QLabel
{
	Q_OBJECT

public:
	explicit MyListItem(QString text)
	{
		setText(text);
		//setAlignment(Qt::AlignCenter);
	}
	~MyListItem()
	{
		
	}
public :
	void setIndex(int index)
	{
		this->index = index;
	}
	int getIndex()
	{
		return index;
	}

	void mousePressEvent(QMouseEvent* e) override
	{
		if (e->button() == Qt::LeftButton)
		{
			emit listItemClicked(index);
		}
	}
signals:
	void listItemClicked(int);
private:
	int index = -1;

};
