#include "MyXmlDelegate.h"
#include <qfile.h>
#include <qdir.h>
#include <qapplication.h>
#include <qdebug.h>
#include <qtextstream.h>
#include <memory>

std::mutex MyXmlDelegate::m_mutex;
MyXmlDelegate * MyXmlDelegate::m_pXmlDelegate = nullptr;

MyXmlDelegate::MyXmlDelegate()
{
	//initStringXML();
}

MyXmlDelegate::~MyXmlDelegate()
{
}

MyXmlDelegate* MyXmlDelegate::getInstance()
{
	if (nullptr == m_pXmlDelegate)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		if (nullptr == m_pXmlDelegate)
		{
			volatile auto temp = new (std::nothrow) MyXmlDelegate();
			m_pXmlDelegate = temp;
		}
	}
	return m_pXmlDelegate;
}

void MyXmlDelegate::initXML()
{

	QDir dir(QCoreApplication::applicationDirPath());
	/*dir.setPath(QCoreApplication::applicationDirPath());*/
	if (!dir.exists("xml"))
	{
		qDebug() << "xml directory doesn`t exist,initialize";
		dir.mkdir("xml");
		
		createXml("strings");
		createXml("resources");

	}
	else
	{
		qInfo() << "xml directory has already existed,if there`s problem reading or writing data into xml, please check if the xml files are there under the xml directory correctly.";
	}
}

void MyXmlDelegate::createXml(QString filename)
{

	QDomDocument doc;
	QDomElement root = doc.createElement(filename);
	QDomElement resources = doc.createElement(filename);

	QDomProcessingInstruction instruction;

	instruction = doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
	doc.appendChild(instruction);

	root.setAttribute("developer", "neko");

	doc.appendChild(root);

	QFile file = QCoreApplication::applicationDirPath() + "/xml/" + filename + ".xml";

	if (!file.open(QIODevice::ReadWrite | QIODevice::Truncate))
	{
		qDebug() << "failed to initialize ./xml/" << filename << ".xml";
	}
	else
	{
		QTextStream stream(&file);
		doc.save(stream, 4);
		file.close();
	}

}
