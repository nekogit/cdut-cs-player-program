#include "MyCMainSpace.h"
#include "MyCSplashScreen.h"
#include "MyCProjectSettingWidget.h"
#include "MySetting.h"
#include "MyCSql.h"
#include "MyCSoftwarePage.h"
#include "MyXmlDelegate.h"


#include <qsplashscreen.h>
#include <qapplication.h>
#include <qdesktopwidget.h>
#include <qsettings.h>
#include <qtextcodec.h>
#include <qfile.h>
#include <qdebug.h>

#include "Utils.h"


//构造函数,展示加载页面和初始化主页面
MyCMainSpace::MyCMainSpace(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MyCMainSpaceClass())
{
	
	ui->setupUi(this);
	showSplash();	//加载页面显示

	_initAll();
	
	setWindowFlag(Qt::Window);

	m_pSplashScreen->finish(this);	//加载页面等待到指定主页面加载完毕
	MyCSplashScreen::getInstance()->destroySplash();	//销毁加载页面
	
	this->show();

}

MyCMainSpace::~MyCMainSpace()
{

	//m_pSettings->setValue("App/pos_x", this->geometry().x());	//主页面在上次关闭时的值
	//m_pSettings->setValue("App/pos_y", this->geometry().y());
	//m_pSettings->setValue("App/win_width", this->width());
	//m_pSettings->setValue("App/win_height", this->height());

	syncAll();

	if (nullptr != m_pProjectSettingWidget)
		delete m_pProjectSettingWidget;

	if (nullptr != m_pMySql)
		delete m_pMySql;

	if (nullptr != m_pMySoftwarePage)
		delete m_pMySoftwarePage;

	/*if (nullptr != m_pMyXml)
		delete m_pMyXml;*/

	delete ui;
}

//QSettings* MyCMainSpace::getSettingInstance()
//{
//	return m_pSettings;	//交给其他页面方便操控
//}

void MyCMainSpace::showSplash()
{
	m_pSplashScreen = MyCSplashScreen::getInstance()->showSplash(":/image/Images/CS2.jpg");
	MyCSplashScreen::getInstance()->setSplashText(QObject::tr("loading"));
	
	//MyCSplashScreen::getInstance()->destroySplash();
}

void MyCMainSpace::_initAll()
{
	initSettings();
	initXml();
	initConnections();
	initSql();
}

void MyCMainSpace::initXml()
{
	MyCSplashScreen::getInstance()->setSplashText(QObject::tr("loading xml file"));
	/*m_pMyXml = new MyXmlDelegate();
	m_pMyXml->initStringXML();*/
	MyXmlDelegate::getInstance()->initXML();
}

void MyCMainSpace::initSettings()
{
	MyCSplashScreen::getInstance()->setSplashText(QObject::tr("loading Appconfig file"));
	//m_pMySetting = new MySetting();
	//MySetting::initSetting();
	m_pSettings = MySetting::getInstance()->initSetting();
	//MySetting::getInstance();
	//m_pSettings = m_pMySetting->initSetting();

	/*m_winAdjust.pos_x = m_pSettings->value("App/pos_x").toInt();
	m_winAdjust.pos_y = m_pSettings->value("App/pos_y").toInt();
	m_winAdjust.win_width = m_pSettings->value("App/win_width").toInt();
	m_winAdjust.win_height = m_pSettings->value("App/win_height").toInt();*/

	/*m_winAdjust.pos_x = m_pMySetting->getInt("App/pos_x");
	m_winAdjust.pos_y = m_pMySetting->getInt("App/pos_y");
	m_winAdjust.win_width = m_pMySetting->getInt("App/win_width");
	m_winAdjust.win_height = m_pMySetting->getInt("App/win_height");*/


	this->setGeometry(systemSetting.pos_x, systemSetting.pos_y, systemSetting.win_width, systemSetting.win_height);


	//QString filename = QCoreApplication::applicationDirPath() + "/AppConfig.ini";

	//m_pSettings = new QSettings(filename, QSettings::IniFormat);	//初始化配置文件

	//m_pSettings->setIniCodec(QTextCodec::codecForName("UTF-8"));

	//if (QFile::exists(filename))
	//{
	//	//qDebug() << m_pSettings->value("System/admin").toString();
	//	if (m_pSettings->value("System/count/isFirstTime").toBool())
	//	{
	//		m_pSettings->setValue("App/pos_x", 0);
	//		m_pSettings->setValue("App/pos_y", 0);
	//		m_pSettings->setValue("App/win_width", 1200);
	//		m_pSettings->setValue("App/win_height", 800);
	//		m_pSettings->setValue("System/count/isFirstTime", false);

	//		m_pSettings->sync();
	//	}
	//	else
	//	{
	//		m_winAdjust.pos_x = m_pSettings->value("App/pos_x").toInt();
	//		m_winAdjust.pos_y = m_pSettings->value("App/pos_y").toInt();
	//		m_winAdjust.win_width = m_pSettings->value("App/win_width").toInt();
	//		m_winAdjust.win_height = m_pSettings->value("App/win_height").toInt();
	//		this->setGeometry(m_winAdjust.pos_x, m_winAdjust.pos_y, m_winAdjust.win_width, m_winAdjust.win_height);
	//	}
	//	
	//}
	//else
	//{
	//	m_pSettings->setValue("System/admin", "CDUT_CS_ADMIN");
	//	m_pSettings->setValue("System/developer", "neko");

	//	m_pSettings->setValue("System/count/isFirstTime", true);

	//	/*m_pSettings->setValue("App/app_width", this->width());
	//	m_pSettings->setValue("App/app_height", this->height());*/

	//	m_pSettings->sync();
	//}
}

void MyCMainSpace::initSql()
{
	MyCSplashScreen::getInstance()->setSplashText(QObject::tr("initialize database"));
	m_pMySql = new MyCSql();
}

void MyCMainSpace::initConnections()
{

	MyCSplashScreen::getInstance()->setSplashText(QObject::tr("initialize connections"));
	if (nullptr == m_pProjectSettingWidget)
		m_pProjectSettingWidget = new MyCProjectSettingWidget(nullptr);

	if (nullptr == m_pMySoftwarePage)
		m_pMySoftwarePage = new MyCSoftwarePage(nullptr);

	connect(ui->actionWindowSettings, &QAction::triggered, this, [&] {
		m_pProjectSettingWidget->show();
		});

	connect(ui->actionsoftwareInfomation, &QAction::triggered, this, [&] {
		m_pMySoftwarePage->show();
		});

	connect(m_pProjectSettingWidget, &MyCProjectSettingWidget::signal_sync, this,&MyCMainSpace::handle_sync_from_setting);
}

void MyCMainSpace::syncAll()
{
	systemSetting.pos_x = this->geometry().x();
	systemSetting.pos_y = this->geometry().y();
	/*m_pMySetting->setInt("App/pos_x", this->geometry().x());
	m_pMySetting->setInt("App/pos_y", this->geometry().y());
	m_pMySetting->setInt("App/win_width", systemSetting.win_width);
	m_pMySetting->setInt("App/win_height", systemSetting.win_height);*/
	/*m_pMySetting->setInt("App/win_height", this->height());*/

	//m_pSettings->sync();	//设置同步
	//MySetting::getInstance()->setInt("App/win_width", 700);
	//m_pMySetting->syncSetting();
	MySetting::getInstance()->syncSetting();
}

void MyCMainSpace::handle_sync_from_setting()
{
	//setFixedSize(m_pMySetting->getInt("App/win_width"),m_pMySetting->getInt("App/win_height"));

	setFixedSize(systemSetting.win_width, systemSetting.win_height);
}

