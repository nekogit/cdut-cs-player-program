#include <iostream>
#include <QApplication>
#include "MyCMainSpace.h"
#include "LogHandler.h"



int main(int argc, char** argv)
{	
	LogHandler::Get().installMessageHandler();

	QApplication app(argc, argv);

	MyCMainSpace s;

	int res = app.exec();
	qInfo() << "initialized application event loop system";

	LogHandler::Get().uninstallMessageHandler();

	return res;
}