#include "MyStyledDelegate.h"
#include <qlineedit.h>

MyStyledDelegate::MyStyledDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{}

MyStyledDelegate::~MyStyledDelegate()
{}

QWidget* MyStyledDelegate::createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	if (index.column() == 1 && index.column() == 2)
	{
		QLineEdit* edit = new QLineEdit(parent);

		edit->setValidator(new QIntValidator(0, 2000, parent));
		return edit;
	}

	return NULL;
}

void MyStyledDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	QString value = index.model()->data(index, Qt::EditRole).toString();
	QLineEdit* edit = static_cast<QLineEdit*>(editor);
	edit->setText(value);
}

void MyStyledDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
	QLineEdit* edit = static_cast<QLineEdit*>(editor);
	model->setData(index, edit->text(), Qt::EditRole);
}

void MyStyledDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	editor->setGeometry(option.rect);
}


