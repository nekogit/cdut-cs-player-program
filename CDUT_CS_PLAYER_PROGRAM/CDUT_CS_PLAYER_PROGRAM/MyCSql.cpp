#include "MyCSql.h"
#include <qssl.h>
#include <qsslsocket.h>

MyCSql::MyCSql()
{
	m_db = QSqlDatabase::addDatabase("QMYSQL", "csSchemaLocal");
	m_db.setHostName("127.0.0.1");
	m_db.setPort(3306);
	//m_db.setDatabaseName("csSchemaLocal");
	m_db.setUserName("root");
	m_db.setPassword("123456");
	m_db.setConnectOptions(QLatin1String("MYSQL_OPT_TLS_VERSION=TLSv1.2"));
	if (!m_db.open())
	{
		qDebug() << "can not connect to database: " << m_db.lastError().text();
		qDebug() << "check if the drivers include the QMYSQL" << QSqlDatabase::drivers();
		qDebug() << "check if the openssl doesn`t loaded" << QSslSocket::supportsSsl() << /*QSslSocket::sslLibraryBuildVersionString() <<*/ QSslSocket::sslLibraryVersionString();
	}
	else
	{
		qInfo() << "success to connect to the database";
	}

	

	m_db.close();
}

MyCSql::~MyCSql()
{
}
