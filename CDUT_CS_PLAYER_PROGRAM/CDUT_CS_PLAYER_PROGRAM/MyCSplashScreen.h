#pragma once
#include <QObject>

class QSplashScreen;
class QMutex;
class QString;

//单例模式
class MyCSplashScreen : public QObject
{
	Q_OBJECT
public:
	/**
		获取该类实例的MyCSplashScreen对象
		以供外界调用其接口
	*/
	static MyCSplashScreen* getInstance();

	/*
		设置在QSplashScreen上显示的图片
	*/
	QSplashScreen* showSplash(const QString &image_name);

	/*
		设置在QSplashScreen上显示的状态信息(文字)
	*/
	void setSplashText(const QString& text);

	/*
		销毁此对象	
	*/
	void destroySplash();
private:
	MyCSplashScreen();
	~MyCSplashScreen();

private:
	QSplashScreen* m_pSplash;
	static QMutex mutex;
	static MyCSplashScreen* m_pSplashScreenMng;
};