#pragma once

#include <QStyledItemDelegate>

class MyStyledDelegate  : public QStyledItemDelegate
{
	Q_OBJECT

public:
	MyStyledDelegate(QObject *parent = 0);
	~MyStyledDelegate();

	QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
	void setEditorData(QWidget* editor, const QModelIndex& index) const override;
	void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const override;
	void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};
