#pragma once

#include <qobject.h>
#include <mutex>
//#include <qsettings.h>
#include <qapplication.h>
#include <qtextcodec.h>
#include <qfile.h>
#include <memory>
//#include <qmutex.h>

#include "MyStruct.h"

extern WinAdjust systemSetting;

//单例模式
class QSettings;
class MySetting : public QObject
{
	Q_OBJECT
public:
	
	QSettings* m_pSetting;

public:
	
	~MySetting();

	static MySetting* getInstance(void);	//保留接口,让其他类直接获取该QSettings对象

	static void deleteInstance();

	QSettings* initSetting();	//当其他类进行构造后,需调用一次此函数以初始化

	void syncSetting();	//同步配置文件,在调用本类的set函数后应使用一次此函数以同步配置文件.

	int getInt(QString text);	//获取以Int为结果的QString:text在配置文件中的值
	void setInt(QString text, int number);	//设置Int为结果的QString:text在配置文件中的值


private:
	MySetting();
	

private:
	//QSettings* m_pSetting;
	
	static MySetting* m_pMySettingMng;
	static std::mutex m_mutex;
};

