#pragma once

#include <qobject.h>
#include <qsqldatabase.h>
#include <qsqlquery.h>
#include <qsqlerror.h>
#include <qdebug.h>

class MyCSql:public QObject
{
	Q_OBJECT
public:
	explicit MyCSql();
	~MyCSql();

private:
	QSqlDatabase m_db;
};

