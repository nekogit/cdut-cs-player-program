#include "MyCSplashScreen.h"

#include <qsplashscreen.h>
#include <qmutex.h>
#include <qstring.h>
#include <qpixmap.h>


QMutex MyCSplashScreen::mutex;
MyCSplashScreen* MyCSplashScreen::m_pSplashScreenMng = NULL;

MyCSplashScreen::MyCSplashScreen()
{
	m_pSplash = nullptr;
}

MyCSplashScreen::~MyCSplashScreen()
{
	
}

MyCSplashScreen* MyCSplashScreen::getInstance()
{
	if (nullptr == m_pSplashScreenMng)
	{
		mutex.lock();
		if (nullptr == m_pSplashScreenMng)
		{
			m_pSplashScreenMng = new MyCSplashScreen();
		}
		mutex.unlock();
	}

	return m_pSplashScreenMng;
}

QSplashScreen* MyCSplashScreen::showSplash(const QString& image_name)
{
	QPixmap pixmap(image_name);
	pixmap = pixmap.scaled(600, 400, Qt::IgnoreAspectRatio , Qt::SmoothTransformation);
	if (pixmap.isNull())
	{
		return NULL;
	}

	m_pSplash = new QSplashScreen(pixmap);
	m_pSplash->setWindowFlag(Qt::WindowStaysOnTopHint);
	m_pSplash->show();
	setSplashText(QObject::tr("��ʼ��..."));

	return m_pSplash;
}

void MyCSplashScreen::setSplashText(const QString& text)
{
	if (nullptr == m_pSplash)
	{
		return;
	}

	QString status = text;
	status += QObject::tr("...");
	m_pSplash->showMessage(status, Qt::AlignLeft | Qt::AlignBottom, Qt::green);
}


void MyCSplashScreen::destroySplash()
{
	if (nullptr != m_pSplash) {
		delete m_pSplash;
		m_pSplash == NULL;
	}
}