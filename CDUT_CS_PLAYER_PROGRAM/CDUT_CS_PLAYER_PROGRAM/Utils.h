#pragma once

#include <qdebug.h>
#include <qstring.h>
#include <qdatetime.h>
#include <qsettings.h>
#include <qapplication.h>
#include <qtextcodec.h>
#include <qfile.h>


#include "MyStruct.h"

namespace Utils {


	
	static inline void Debug(QString message)
	{
		QDateTime dateTime = QDateTime::currentDateTime();

		qDebug() << dateTime.toString("yyyy-MM-dd hh:mm:ss:zzz").toStdString().c_str()
			<< QString(" [Debug]: ").toStdString().c_str()
			<< message.toStdString().c_str();
	}

	static inline void WARNING(QString message)
	{
		QDateTime dateTime = QDateTime::currentDateTime();

		qDebug() << dateTime.toString("yyyy-MM-dd hh:mm:ss:zzz").toStdString().c_str()
			<< QString(" [WARNING]: ").toStdString().c_str()
			<< message.toStdString().c_str();
	}

	static inline void ERROR(QString message)
	{
		QDateTime dateTime = QDateTime::currentDateTime();

		qDebug() << dateTime.toString("yyyy-MM-dd hh:mm:ss:zzz").toStdString().c_str()
			<< QString(" [ERROR]: ").toStdString().c_str()
			<< message.toStdString().c_str();
	}

	static inline void INFO(QString message)
	{
		QDateTime dateTime = QDateTime::currentDateTime();

		qDebug() << dateTime.toString("yyyy-MM-dd hh:mm:ss:zzz").toStdString().c_str()
			<< QString(" [INFO]: ").toStdString().c_str()
			<< message.toStdString().c_str();
	}



}

