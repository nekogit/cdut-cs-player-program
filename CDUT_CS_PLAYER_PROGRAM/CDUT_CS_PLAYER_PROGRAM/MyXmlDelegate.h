#pragma once

#include <QtXml>
#include <qdom.h>
#include <qxmlstream.h>
#include <mutex>

class MyXmlDelegate
{
public:
	static MyXmlDelegate* getInstance();

	void initXML();
private:

	void createXml(QString file);
	
	MyXmlDelegate();
	~MyXmlDelegate();

private:

	QXmlStreamReader m_xReader;
	QXmlStreamWriter m_xWriter;
	//QDomDocument m_doc;

	static MyXmlDelegate* m_pXmlDelegate;
	static std::mutex m_mutex;
};

