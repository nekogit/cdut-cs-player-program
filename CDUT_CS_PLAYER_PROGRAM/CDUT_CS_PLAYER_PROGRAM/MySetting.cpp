#include "MySetting.h"
//#include <mutex>
#include "qsettings.h"
//#include "mutex"

WinAdjust systemSetting = { 0,0,1200,1000 };

std::mutex MySetting::m_mutex;
MySetting* MySetting::m_pMySettingMng = nullptr;

MySetting::MySetting()
{
	//initSetting();
	//QString filename = QCoreApplication::applicationDirPath() + "/config/AppConfig.ini";
	m_pSetting = nullptr;

}

MySetting::~MySetting()
{
	/*if (m_pSetting != nullptr)
		delete m_pSetting;*/
}


MySetting* MySetting::getInstance(void)
{
	if (nullptr == m_pMySettingMng)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		if (nullptr == m_pMySettingMng)
		{
			volatile auto temp = new (std::nothrow) MySetting();
			m_pMySettingMng = temp;
		}
		//m_mutex.unlock();
	}

	return m_pMySettingMng;
}

void MySetting::deleteInstance()
{
	std::unique_lock<std::mutex> lock(m_mutex);

	if (m_pMySettingMng)
	{
		delete m_pMySettingMng;
		m_pMySettingMng = nullptr;
	}
}

QSettings* MySetting::initSetting()
{
	QString filename = QCoreApplication::applicationDirPath() + "/config/AppConfig.ini";

	////m_pSetting = new QSettings(filename, QSettings::IniFormat);	//��ʼ�������ļ�
	m_pSetting = new QSettings(filename);
	m_pSetting->setDefaultFormat(QSettings::IniFormat);

	m_pSetting->setIniCodec(QTextCodec::codecForName("UTF-8"));

	if (QFile::exists(filename))
	{
		//qDebug() << m_pSettings->value("System/admin").toString();
		if (m_pSetting->value("System/count/isFirstTime").toBool())
		{
			m_pSetting->setValue("App/pos_x", 0);
			m_pSetting->setValue("App/pos_y", 0);
			m_pSetting->setValue("App/win_width", 1200);
			m_pSetting->setValue("App/win_height", 1000);
			m_pSetting->setValue("System/count/isFirstTime", false);

			m_pSetting->sync();
		}
		else
		{
			systemSetting.pos_x = getInt("App/pos_x");
			systemSetting.pos_y = getInt("App/pos_y");
			systemSetting.win_width = getInt("App/win_width");
			systemSetting.win_height = getInt("App/win_height");
		}

	}
	else
	{
		m_pSetting->setValue("System/admin", "CDUTCSADMIN");
		m_pSetting->setValue("System/developer", "neko");

		m_pSetting->setValue("System/count/isFirstTime", true);

		/*m_pSettings->setValue("App/app_width", this->width());
		m_pSettings->setValue("App/app_height", this->height());*/

		m_pSetting->sync();
	}

	/*systemSetting.pos_x = getInt("App/pos_x");
	systemSetting.pos_y = getInt("App/pos_y");
	systemSetting.win_width = getInt("App/win_width");
	systemSetting.win_height = getInt("App/win_height");*/

	return m_pSetting;
}

void MySetting::syncSetting()
{

	setInt("app/pos_x", systemSetting.pos_x);
	setInt("app/pos_y", systemSetting.pos_y);
	setInt("app/win_width", systemSetting.win_width);
	setInt("app/win_height", systemSetting.win_height);


	m_pSetting->sync();

	/*return m_pSetting;*/
}

int MySetting::getInt(QString text)
{
	int temp = m_pSetting->value(text,"-1").toInt();
	return temp;
}

void MySetting::setInt(QString text, int number)
{
	m_pSetting->setValue(text, number);
}
